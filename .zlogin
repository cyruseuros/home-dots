# add some privacy
umask 037

# colorize tty
source ~/.cache/wal/colors-tty.sh

# start recoll re-indexing daemon
recollindex -mx &> /dev/null
