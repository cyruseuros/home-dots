import os
import subprocess

# interactive config
config.load_autoconfig()

# enable js.
config.set('content.javascript.enabled', True, 'file://*')
config.set('content.javascript.enabled', True, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')

# qutewal
qutewal_repo = r'https://gitlab.com/jjzmajic/qutewal.git'
qutewal_file = os.path.expanduser('~/projects/qutewal/qutewal.py')
qutewal_dir = os.path.dirname(qutewal_file)
if not os.path.isfile(qutewal_file):
    os.makedirs(qutewal_dir)
    subprocess.Popen(['git', 'clone', qutewal_repo, qutewal_dir])
config.source(qutewal_file)

# spelling
c.spellcheck.languages = ['en-US']

# passwords
config.bind(',l', 'spawn --userscript qute-pass')
config.bind(',ul', 'spawn --userscript qute-pass --username-only')
config.bind(',pl', 'spawn --userscript qute-pass --password-only')
config.bind(',ol', 'spawn --userscript qute-pass --otp-only')

# jupyter notebook vim key conflicts
config.unbind('<Shift-Escape>', mode='passthrough')
config.bind('<Alt-Escape>', 'leave-mode', mode='passthrough')
config.unbind('<Ctrl-v>', mode='normal')
config.bind('<Alt-v>', 'enter-mode passthrough', mode='normal')

# editor
c.editor.command = ['emacsclient', '-ca ''', '{}']

# fonts
font =  'Source Code Pro Semibold'
font_style =  '10.5pt monospace'
c.fonts.monospace = font
c.fonts.completion.category = font_style
c.fonts.completion.entry = font_style
c.fonts.debug_console = font_style
c.fonts.downloads = font_style
c.fonts.hints = font_style
c.fonts.keyhint = font_style
c.fonts.messages.error = font_style
c.fonts.messages.info = font_style
c.fonts.messages.warning = font_style
c.fonts.prompts = font_style
c.fonts.statusbar = font_style
c.fonts.tabs = font_style

# placement
c.downloads.position = 'bottom'

# search
c.url.start_pages = ['https://start.duckduckgo.com/']
c.url.searchengines = {
    'DEFAULT': 'https://www.duckduckgo.com/?q={}',
    'g': 'http://www.google.com/search?q={}',
    'a': 'https://wiki.archlinux.org/?search={}',
}

# spoof user agent new chrome
c.content.headers.user_agent = \
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 ' + \
    '(KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'
